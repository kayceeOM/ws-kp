package gigster_eng.workspace.views.activities;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import gigster_eng.workspace.R;
import gigster_eng.workspace.views.fragments.ChooseUserTypeFragment;

public class AuthActivity extends FragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);
        ChooseUserTypeFragment chooseUserTypeFragment = new ChooseUserTypeFragment();
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.fragment_container, chooseUserTypeFragment)
                .commit();
    }

    public void switchFragment(Fragment fragment) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_container, fragment)
                .addToBackStack(null)
                .commit();
    }
}
