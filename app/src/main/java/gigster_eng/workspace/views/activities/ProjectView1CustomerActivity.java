package gigster_eng.workspace.views.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import gigster_eng.workspace.R;

public class ProjectView1CustomerActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project_view1_customer);

        View milestonesTab = findViewById(R.id.milestones_tab);
        milestonesTab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openMilestonesActivity();
            }
        });

        View subchatTab = findViewById(R.id.subchat_tab);
        subchatTab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openSubchatActivity();
            }
        });

        View requestChangeTab = findViewById(R.id.request_change_tab);
        requestChangeTab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openNewChangeRequestActivity();
            }
        });

        View requestUpdateTab = findViewById(R.id.request_update_tab);
        requestUpdateTab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openCustomerChangeRequestActivity();
            }
        });
    }

    public void openNewChangeRequestActivity(){
        Intent i = new Intent(ProjectView1CustomerActivity.this, NewCustomerChangeRequest.class);
        startActivity(i);
    }

    public void openSubchatActivity(){
        Intent i = new Intent(ProjectView1CustomerActivity.this, CustomerUpdateChatActivity.class);
        startActivity(i);
    }

    public void openMilestonesActivity(){
        Intent i = new Intent(ProjectView1CustomerActivity.this, MilestonesActivity.class);
        startActivity(i);
    }

    public void openCustomerChangeRequestActivity(){
        Intent i = new Intent(ProjectView1CustomerActivity.this, CustomerChangeRequestManager.class);
        startActivity(i);
    }


}
