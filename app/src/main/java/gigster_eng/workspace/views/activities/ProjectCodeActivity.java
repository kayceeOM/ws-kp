package gigster_eng.workspace.views.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import gigster_eng.workspace.R;


public class ProjectCodeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project_code);

        Button continueBtn = (Button) findViewById(R.id.continue_btn);
        continueBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openProjectViewActivity();
            }
        });
    }

    public void openProjectViewActivity(){
        Intent i = new Intent(ProjectCodeActivity.this, ProjectView1CustomerActivity.class);
        startActivity(i);
    }
}
