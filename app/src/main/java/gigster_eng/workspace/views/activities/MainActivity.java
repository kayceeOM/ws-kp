package gigster_eng.workspace.views.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import gigster_eng.workspace.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

}
