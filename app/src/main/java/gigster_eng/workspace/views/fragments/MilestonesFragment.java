package gigster_eng.workspace.views.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import gigster_eng.workspace.R;
import gigster_eng.workspace.views.activities.AddProjectActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class MilestonesFragment extends Fragment {


    public MilestonesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_milestones, container, false);
        Button next = (Button) view.findViewById(R.id.next);

        next.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                getActivity().finish();
            }
        });

        return view;
    }

}
