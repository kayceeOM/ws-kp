package gigster_eng.workspace.views.fragments;


import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import gigster_eng.workspace.R;
import gigster_eng.workspace.views.activities.AuthActivity;
import gigster_eng.workspace.views.activities.ContractorProjectsActivity;
import gigster_eng.workspace.views.activities.MainActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class LoginFragment extends Fragment {

    SharedPreferences prefs;


    public LoginFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_login, container, false);
        prefs = getActivity().getSharedPreferences(getActivity().getPackageName(), getActivity().MODE_PRIVATE);

        Button googleButton = (Button) v.findViewById(R.id.google);
        googleButton.setOnClickListener(new ClickListener());

        Button fbButton = (Button) v.findViewById(R.id.facebook);
        fbButton.setOnClickListener(new ClickListener());

        Button emailButton = (Button) v.findViewById(R.id.email);
        emailButton.setOnClickListener(new ClickListener());

        return v;
    }

    public class ClickListener implements View.OnClickListener{

        @Override
        public void onClick(View view) {
            if(prefs.getBoolean("inContractorMode", false)){
                startActivity(new Intent(getActivity(), ContractorProjectsActivity.class));
            }else{
                ((AuthActivity) getActivity()).switchFragment(new WalkthroughFragment());
            }

        }
    }

}
