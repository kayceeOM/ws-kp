package gigster_eng.workspace.views.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.daimajia.slider.library.Indicators.PagerIndicator;
import com.daimajia.slider.library.SliderLayout;

import org.w3c.dom.Text;

import gigster_eng.workspace.R;
import gigster_eng.workspace.utils.TextSliderView;
import gigster_eng.workspace.views.activities.ProjectCodeActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class WalkthroughFragment extends Fragment {

    private SliderLayout slider;
    TextSliderView walkthroughSlider1, walkthroughSlider2, walkthroughSlider3, walkthroughSlider4;


    public WalkthroughFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_walkthrough, container, false);
        slider = (SliderLayout)v.findViewById(R.id.slider);

        initializeSliders();
        slider.setCustomIndicator((PagerIndicator)v.findViewById(R.id.custom_indicator));

        TextView skipBtn = (TextView) v.findViewById(R.id.go_to_workspace);
        skipBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openProjectCodeActivity();
            }
        });

        return v;
    }

    public void initializeSliders(){

        walkthroughSlider1 = new TextSliderView(getActivity());
        walkthroughSlider2 = new TextSliderView(getActivity());
        walkthroughSlider3 = new TextSliderView(getActivity());
        walkthroughSlider4 = new TextSliderView(getActivity());


        walkthroughSlider1.description("Receive real time updates from your contractor for all of your home improvement projects")
                .image(R.drawable.bg_walkthrough_1);

        walkthroughSlider2.description("Keep track of project milestones, communicate changes or upload a punch list for your contractor")
                .image(R.drawable.bg_walkthrough_2);

        walkthroughSlider3.description("Approve changes to project plans and review all of your project documentation")
                .image(R.drawable.bg_walkthrough_3);

        walkthroughSlider4.description("Monitor your project budget and send payments at identified intervals")
                .image(R.drawable.bg_walkthrough_4);


        slider.addSlider(walkthroughSlider1);
        slider.addSlider(walkthroughSlider2);
        slider.addSlider(walkthroughSlider3);
        slider.addSlider(walkthroughSlider4);
    }

    public void openProjectCodeActivity(){
        Intent i = new Intent(getActivity(), ProjectCodeActivity.class);
        startActivity(i);
    }

    @Override
    public void onStop() {
        slider.stopAutoCycle();
        super.onStop();
    }
}
