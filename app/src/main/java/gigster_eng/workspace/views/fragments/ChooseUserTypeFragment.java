package gigster_eng.workspace.views.fragments;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import gigster_eng.workspace.R;
import gigster_eng.workspace.views.activities.AuthActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChooseUserTypeFragment extends Fragment {
    private Button contractorButton, homeownerButton;
    private AuthActivity authActivity;
    SharedPreferences prefs;

    public ChooseUserTypeFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_choose_user_type, container, false);

        prefs = getActivity().getSharedPreferences(getActivity().getPackageName(), getActivity().MODE_PRIVATE);

        initializeViews(v);
        return v;
    }

    private void initializeViews(View v) {
        authActivity = (AuthActivity) getActivity();
        contractorButton = (Button) v.findViewById(R.id.contractor_button);
        homeownerButton = (Button) v.findViewById(R.id.homeowner_button);
        setButtonListeners();
    }

    private void setButtonListeners() {
        homeownerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                prefs.edit().putBoolean("inContractorMode", false).apply();
                registerOrLogIn();
            }
        });

        contractorButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                prefs.edit().putBoolean("inContractorMode", true).apply();
                registerOrLogIn();
            }
        });
    }

    private void registerOrLogIn() {
        authActivity.switchFragment(new RegisterOrLogInFragment());
    }
}
