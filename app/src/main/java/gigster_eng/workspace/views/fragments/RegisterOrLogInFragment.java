package gigster_eng.workspace.views.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import gigster_eng.workspace.R;
import gigster_eng.workspace.views.activities.AuthActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class RegisterOrLogInFragment extends Fragment {
    private Button registerButton, loginButton;
    private AuthActivity authActivity;


    public RegisterOrLogInFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_register_or_log_in, container, false);
        initializeViews(v);
        return v;
    }


    private void initializeViews(View v) {
        authActivity = (AuthActivity) getActivity();
        registerButton = (Button) v.findViewById(R.id.create_account);
        loginButton = (Button) v.findViewById(R.id.login);
        setButtonListeners();
    }

    private void setButtonListeners() {
        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                displayRegisterView();
            }

        });

        loginButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                displayLoginView();
            }
        });
    }

    private void displayLoginView(){
        authActivity.switchFragment(new LoginFragment());
    }

    private void displayRegisterView(){
        authActivity.switchFragment(new RegisterFragment());
    }


}
