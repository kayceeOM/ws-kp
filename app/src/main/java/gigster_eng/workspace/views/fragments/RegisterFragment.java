package gigster_eng.workspace.views.fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import gigster_eng.workspace.R;
import gigster_eng.workspace.views.activities.AuthActivity;
import gigster_eng.workspace.views.activities.ContractorProjectsActivity;

/**
 * A simple {@link Fragment} subclass.
 */

public class RegisterFragment extends Fragment {

    SharedPreferences prefs;

    public RegisterFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_register, container, false);

        Button googleButton = (Button) v.findViewById(R.id.google);
        googleButton.setOnClickListener(new ClickListener());

        Button fbButton = (Button) v.findViewById(R.id.facebook);
        fbButton.setOnClickListener(new ClickListener());

        Button emailButton = (Button) v.findViewById(R.id.email);
        emailButton.setOnClickListener(new ClickListener());

        return v;
    }

    public class ClickListener implements View.OnClickListener{

        @Override
        public void onClick(View view) {
            if(prefs.getBoolean("inContractorMode", false)){
                startActivity(new Intent(getActivity(), ContractorProjectsActivity.class));
            }else{
                ((AuthActivity) getActivity()).switchFragment(new WalkthroughFragment());
            }

        }
    }
}
